# Changelog

## Unreleased

## 2.2.0 - 2024-10-07
### Fixed

- #6 fix: envoyer une notification aux responsables lorsque le texte d'un article est effacé
