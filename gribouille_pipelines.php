<?php
/**
 * Plugin / Squelettes Gribouille
 * © Fil
 * Distribue sous licence GNU/GPL
 */

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Insertion dans le pipeline post_edition
 * Envoyer une notification aux webmestres
 * lorsqu'un article est modifié et que son texte a été totalement effacé
 *
 * @param array $flux
 *
 * @return array
 */
function gribouille_post_edition( $flux ) {

	if( isset( $flux['args']['action'] )
		&& ( $flux['args']['action'] == 'modifier' )
		&& isset( $flux['args']['table'] )
		&& $flux['args']['table'] == 'spip_articles'
		&& isset( $flux['args']['id_objet'] )
		&& isset( $flux['data']['texte'] )
	) {
		
		$objet = 'article';
		$id_objet = $flux['args']['id_objet'];
		$serveur = $flux['args']['serveur'];

		$row = sql_fetsel( 'statut,id_rubrique,id_secteur', 'spip_articles', 'id_article='.intval($id_objet), '', '', '', '', $serveur );

		if( $row ) {
			$statut = $row['statut'];
			$id_rubrique = $row['id_rubrique'];
			$id_secteur = $row['id_secteur'];
			$texte = $flux['data']['texte'];
		}

		$secteur_wiki = lire_config( 'gribouille/secteur_wiki' );
		$strlen_texte = strlen( $texte );

		//inutile de poursuivre
		if( $statut != 'publie' || $id_secteur != $secteur_wiki || $strlen_texte > 0 ) {
			return $flux;
		}

		if ($notifications = charger_fonction('notifications', 'inc')) {

			$notifications( 
				'article_vide',
				$id_objet,
				[
					'objet' => 'article',
					'id_objet' => $id_objet,
					'id_rubrique' => $id_rubrique,
					'id_parent' => $id_rubrique,
					'id_secteur' => $id_secteur
				]
			);
		}

	}
	return $flux;
}

/**
 * Insertion dans le pipeline notifications_destinataires
 * Ajoute des destinataires dans les notifications
 *
 * @param array $flux : le contexte du pipeline
 * @return array $flux : le contexte modifié
 */
function gribouille_notifications_destinataires($flux) {

	if( $flux['args']['quoi'] == 'article_vide' ) {

		$destinataires_flux = $flux['data'];
		$destinataires = [];

		$id = $flux['args']['id'];
		$id_rubrique = $flux['args']['options']['id_rubrique'];
		$id_secteur = $flux['args']['options']['id_secteur'];

		// Notifier les auteur*es de l'article
		$destinataires_article = array_column( objet_trouver_liens( [ 'id_auteur' => '*' ], [ 'article' => intval( $id ) ] ), 'id_auteur' );

		// + les admins de la rubrique
		$destinataires_admin = array_column( objet_trouver_liens( [ 'id_auteur' => '*' ], [ 'rubrique' => intval( $id_secteur ), 'statut' => '0minirezo' ] ), 'id_auteur' );

		// + les admins du secteur
		$destinataires_admin_secteur = array_column( objet_trouver_liens( [ 'id_auteur' => '*' ], [ 'rubrique' => intval( $id_secteur ), 'statut' => '0minirezo' ] ), 'id_auteur' );

		$destinataires = array_merge( $destinataires_flux, $destinataires_article, $destinataires_admin, $destinataires_admin_secteur );

		// sinon les webmestres du site
		if( count($destinataires) === 0 ) {
			$destinataires = array_column( sql_allfetsel('id_auteur', 'spip_auteurs', "statut='0minirezo' AND webmestre='oui'") , 'id_auteur' );
		}

		$flux['data'] = array_unique($destinataires);

	}

	return $flux;
}

/**
 * Insertion dans le pipeline Styliser
 * définir le squelette a utiliser si on est dans le cas
 * d'une rubrique de Gribouille
 *
 * @param array $flux
 *
 * @return array
 */
function gribouille_styliser($flux) {
	// Gribouille ne s'active que sur les rubriques et les articles
	if (($fond = $flux['args']['fond'])
		and in_array($fond, [
			'article',
			'rubrique',
		])) {

		$ext = $flux['args']['ext'];

		// Si la rubrique fait partie du secteur défini dans la configuration on change son fond
		if ($id_rubrique = $flux['args']['id_rubrique']) {
			$id_secteur = sql_getfetsel('id_secteur', 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique));
			if (in_array($id_secteur, gribouille_secteurs_wiki())) {
				include_spip('inc/autoriser');
				// On vérifie si nous sommes autorisé à voir le Wiki
				//if (autoriser('voir', 'rubrique', $id_rubrique, $GLOBALS['visiteur_session'])) {
				if ($squelette = test_squelette_gribouille($fond, $ext)) {
					$flux['data'] = $squelette;
					// définir les intertitres en H2 sur le Wiki
					$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\">\n";
					$GLOBALS['fin_intertitre'] = "</h2>\n";
				}
				//				} else {
				//					unset($flux['args']);
				//				}
			}
		}
	}

	return $flux;
}

/**
 * Fonction qui vérifie la présence d'un squelette gribouille disponible
 *
 * @param string $fond article ou rubrique
 * @param string $ext  extension des squelettes (html)
 *
 * @return mixed
 */
function test_squelette_gribouille($fond, $ext) {
	if ($squelette = find_in_path($fond . "_gribouille.$ext")) {
		return substr($squelette, 0, -strlen(".$ext"));
	}

	return false;
}

/**
 * Insertion dans le pipeline Prepare Recherche
 * Si configuré comme tel, exclure de la recherche les secteurs de gribouille
 *
 * @param array $flux
 *
 * @return array
 */
function gribouille_prepare_recherche($flux) {
	if (lire_config('gribouille/exclure_recherche') == 'on') {
		$id_secteurs_wiki = gribouille_secteurs_wiki();
		if (is_array($id_secteurs_wiki)) {
			if ($flux['args']['type'] == 'article'
				and $flux['args']['tout'] == '0'
				and $points = $flux['data']) {
				$serveur = $flux['args']['serveur'];
				$p2 = [];
				$s = sql_select(
					"id_article",
					"spip_articles",
					"statut='publie'
						AND " . sql_in('id_article', array_keys($points), '', '', '', '', $serveur) . "
						AND " . sql_in('id_secteur', array_values($id_secteurs_wiki), 'NOT', '', '', '', $serveur)
				);
				while ($t = sql_fetch($s, $serveur)) {
					$p2[$t['id_article']] = '';
				}
				$p2 = array_intersect_key($points, $p2);
				$flux['data'] = $p2;
			}
			if ($flux['args']['type'] == 'rubrique'
				and $flux['args']['tout'] == '0'
				and $points = $flux['data']) {
				$serveur = $flux['args']['serveur'];
				$p2 = [];
				$s = sql_select(
					"id_rubrique",
					"spip_rubriques",
					"statut='publie'
						AND " . sql_in('id_rubrique', array_keys($points), '', '', '', '', $serveur) . "
						AND " . sql_in('id_secteur', array_values($id_secteurs_wiki), 'NOT', '', '', '', $serveur)
				);
				while ($t = sql_fetch($s, $serveur)) {
					$p2[$t['id_rubrique']] = '';
				}
				$p2 = array_intersect_key($points, $p2);
				$flux['data'] = $p2;
			}
		}
	}

	return $flux;
}

