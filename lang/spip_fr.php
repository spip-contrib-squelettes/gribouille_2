<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/spip.git
return [
	// I
	'info_effacement_article' => '[@nom_site_spip@] Effacement de : @titre@',
	'info_effacement_article_2' => 'L’article vient d’être modifié et son texte totalement effacé.
---------------',
	'info_effacement_article_3' => 'L’article "@titre@" a été effacé.',
	'info_effacement_article_4' => 'Vous êtes invité à vérifier son état et à restaurer sa version antérieure (si les révisions sont actives).',
];